import { Component, OnInit } from '@angular/core';
import { AuthService} from '../auth.service';
import { Router } from "@angular/router";

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  logout(){
    this.authService.logout().
    then(value =>{
      this.router.navigate(['/login'])
    }).catch(err=> {console.log(err)})

  }

  constructor(private router: Router , private authService:AuthService) { }

  ngOnInit() {
  }

}
