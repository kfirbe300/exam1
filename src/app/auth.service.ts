import { Injectable } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { AngularFireAuth} from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  login(email:string, password:string){
    return this.
    fireBaseAuth.
    auth.
    signInWithEmailAndPassword(email,password);

  }

  logout(){
    return this.fireBaseAuth
    .auth.signOut();
  }
  user: Observable<firebase.User>;

  constructor(private fireBaseAuth: AngularFireAuth , private db: AngularFireDatabase) {

    this.user = fireBaseAuth.authState;
   }
}
